﻿using System.Collections.Generic;

namespace Wezit.DataModels.DTO
{
    public class TruckTypeDTO
    {
        public int TruckTypeId { get; set; }
        public string TruckTypeName { get; set; }

        public virtual ICollection<TruckDTO> Trucks { get; set; }
    }
}
