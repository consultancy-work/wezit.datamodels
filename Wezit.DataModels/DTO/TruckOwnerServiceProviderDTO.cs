﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class TruckOwnerServiceProviderDTO
    {
        public int TruckOwnerProviderId { get; set; }
        public int TruckOwnerId { get; set; }
        public int ServiceProviderId { get; set; }
        public string ServiceProviderName { get; set; }
        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; }
    }
}
