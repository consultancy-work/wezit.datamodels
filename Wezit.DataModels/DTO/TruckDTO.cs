using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TruckDTO
    {
        public int TruckId { get; set; }
        [Required]
        public int TruckCompanyId { get; set; }
        [Required]
        public string RegistrationNumber { get; set; }
        [Required]
        public int Capacity { get; set; }

        public int TruckTypeId { get; set; }

        public int CurrentLocationId { get; set; }

        public string TruckImeiNumber { get; set; }

        public TruckOwnerDTO TruckCompany { get; set; }
        
        public TruckTypeDTO TruckType { get; set; }

        public ICollection<TruckBookingDTO> TruckBookings { get; set; }

        public ICollection<TrucksSubscribedDTO> TrucksSubscribeds { get; set; }
    }
}