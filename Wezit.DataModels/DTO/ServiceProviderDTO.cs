﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class ServiceProviderDTO
    {
        public int ServiceProviderId { get; set; }
        public string ServiceProviderTypeCode { get; set; }
        public string ServiceProviderName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
