using System;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TruckBookingEventLogDTO
    {
        public int EventId { get; set; }
        [Required]
        public int BookingId { get; set; }
        [Required]
        public int EventStageId { get; set; }

        public TruckBookingDTO Booking { get; set; }

        public EventLogStageDTO EventStage { get; set; }
    }
}