﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class CustomerServiceProviderDTO
    {
        public int CustomerProviderId { get; set; }
        public int CustomerId { get; set; }
        public int ServiceProviderId { get; set; }
        public int BookingId { get; set; }
        public string ServiceProviderName { get; set; } 
        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; }
    }
}
