using System.Collections.Generic;

namespace Wezit.DataModels.DTO
{
    public class EventLogStageDTO
    {
        public int StageId { get; set; }

        public string Description { get; set; }

        public ICollection<TruckBookingEventLogDTO> TruckBookingEventLogs { get; set; }
    }
}