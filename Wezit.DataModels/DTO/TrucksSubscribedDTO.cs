using System;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TrucksSubscribedDTO
    {
        public int SubscriptionId { get; set; }
        [Required]
        public int TruckId { get; set; }
        [Display(Name = "Approved")]
        public bool Approved { get; set; }
        public int TruckSubscriptionId { get; set; }

        public TruckDTO Truck { get; set; }
    }
}