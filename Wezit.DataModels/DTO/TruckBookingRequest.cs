﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class TruckBookingRequest
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public int TruckTypeId { get; set; }
        public long CustomerId { get; set; }
        public decimal LoadCapacity { get; set; }
        public int LoadTypeId { get; set; }
        public int MotorClinicProviderServiceId { get; set; }
    }
}
