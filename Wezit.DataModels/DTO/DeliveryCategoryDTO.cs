﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class DeliveryCategoryDTO
    {
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
            public decimal PricePerUnit { get; set; }

            public virtual ICollection<TruckBookingDTO> TruckBookings { get; set; }
        
    }
}
