﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class ServiceProviderTypeDTO
    {
        public string ProviderTypeCode { get; set; }
        public string ProviderTypeName { get; set; }
    }
}
