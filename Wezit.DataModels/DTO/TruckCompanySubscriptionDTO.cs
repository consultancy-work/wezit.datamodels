using System;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TruckCompanySubscriptionDTO
    {
        public int TruckSubscriptionId { get; set; }

        public int TruckCompanyId { get; set; }

        public int BandId { get; set; }

        [Required]
        public DateTime SubscriptionStartDate { get; set; }
        [Required]
        public DateTime SubscriptionEndDate { get; set; }

        public BandSubscriptionDTO Band { get; set; }

        public TruckOwnerDTO TruckCompany { get; set; }
    }
}