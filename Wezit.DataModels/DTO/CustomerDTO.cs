using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "First name must be at least 2 characters or more")]
        [MaxLength(40, ErrorMessage = "First name must not exceed 40 characters")]
        public string Firstname { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Last name must be at least 2 characters or more")]
        [MaxLength(40, ErrorMessage = "Last name must not exceed 40 characters")]
        public string Lastname { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(40, ErrorMessage = "Email address length must not exceed 80 characters")]
        public string EmailAddress { get; set; }

        [Required]
        [Phone]
        [MaxLength(13, ErrorMessage = "Phone number length must not exceed 13 characters")]
        public string PhoneNumber { get; set; }

        public string PostalAddress { get; set; }

        public string PhysicalAddress { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        [MaxLength(60, ErrorMessage = "City length must not exceed 60 characters")]
        public string City { get; set; }
        
        public bool IsCompany { get; set; }

        [MinLength(3, ErrorMessage = "Company name must be at least 3 characters or more")]
        [MaxLength(90, ErrorMessage = "Company name must not exceed 90 characters")]
        public string CompanyName { get; set; }
        public string CompanyRegDocPath { get; set; }
        public string CustomerIdPath { get; set; }
        public bool IsVerified { get; set; }

        public ICollection<TruckBookingDTO> TruckBookings { get; set; }
    }
}