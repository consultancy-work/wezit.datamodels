using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TruckBookingDTO
    {
        public int TruckBookingId { get; set; }

        [Required]
        public int TruckId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        public int? CurrentLocationId { get; set; }

        [Required]
        public DateTime LastUpdated { get; set; }

        public string LoadDescription { get; set; }

        public double LoadCapacity { get; set; }
        public string SourceLocation { get; set; }
        public string DestinationLocation { get; set; }


        public CustomerDTO Customer { get; set; }

        public TruckDTO Truck { get; set; }

        public int LoadCategoryId { get; set; }

        public DeliveryCategoryDTO LoadCategory { get; set; }

        public int? MotorClinicProviderServiceId { get; set; }
        public string MotorClinicProviderServiceName { get; set; }

        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; }
        public IEnumerable<ServiceProviderDTO> MotorClinic { get; set; }
        public ICollection<TruckBookingEventLogDTO> TruckBookingEventLogs { get; set; }

    }
}