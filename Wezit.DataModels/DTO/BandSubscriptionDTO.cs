using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class BandSubscriptionDTO
    {
        [Display(Name = "Band Id")]
        public int BandId { get; set; }
        [Required]
        [Display(Name = "Band Name")]
        public string BandName { get; set; }
        [Display(Name = "Band Range")]
        public string BandRange { get; set; }

        [Display(Name = "Band Cap")]
        [Required]
        public string BandCap { get; set; }
        [Display(Name = "Price")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        [Required]
        public decimal Cost { get; set; }

        public ICollection<TruckCompanySubscriptionDTO> TruckCompanySubscriptions { get; set; }
    }
}