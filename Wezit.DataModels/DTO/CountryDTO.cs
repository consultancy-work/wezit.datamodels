﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class CountryDTO
    {
        public int CountryId { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "Country must be at least 3 characters or more")]
        [MaxLength(60, ErrorMessage = "Country must not exceed 60 characters")]
        public string CountryName { get; set; }
    }
}
