﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wezit.DataModels.DTO
{
    public class VerificationCodeDTO
    {
        public long CodeId { get; set; }
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
    }
}
