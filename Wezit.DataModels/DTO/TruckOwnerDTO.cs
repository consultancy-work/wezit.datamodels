using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wezit.DataModels.DTO
{
    public class TruckOwnerDTO
    {
        public int TruckCompanyId { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(80, ErrorMessage = "An email address cannot go over 80 characters")]
        public string EmailAddress { get; set; }

        [Required]
        [Phone]
        [MaxLength(13, ErrorMessage = "Contact number length must not exceed 13 characters")]
        public string ContactNumber { get; set; }

        [Required]
        public string ContactPersonFirstName { get; set; }

        [Required]
        public string ContactPersonLastName { get; set; }

        public string PhysicalAddress { get; set; }

        public string PostalAddress { get; set; }
        [Required]
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        [Required]
        [MaxLength(60, ErrorMessage = "City length must not exceed 60 characters")]
        public string City { get; set; }

        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; }
        public ICollection<TruckCompanySubscriptionDTO> TruckCompanySubscriptions { get; set; }

        public ICollection<TruckDTO> Trucks { get; set; }
    }
}