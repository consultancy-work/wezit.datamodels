﻿using System.Collections.Generic;

namespace Wezit.DataModels.General
{
    public class FeedbackMessage
    {
        public bool HasErrorOccured;

        public string Message;

        public string FeedbackType;

        public IList<string> Messages;

        public object Data;
    }
}
