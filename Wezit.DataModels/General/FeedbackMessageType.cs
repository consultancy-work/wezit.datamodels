﻿namespace Wezit.DataModels.General
{
    public static class FeedbackMessageType
    {
        public const string INFORMATION = "Information";

        public const string SUCCESS = "Success";

        public const string GENERALERROR = "GeneralError";

        public const string EXCEPTIONERROR = "ExceptionError";

        public const string VALIDATIONERROR = "ValidationError";
    }
}
