﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;

namespace Wezit.DataModels.ViewModels
{
    public class ServiceProviderViewModel : ServiceProviderDTO
    {
        public IEnumerable<ServiceProviderTypeDTO> ServiceProviderTypes { get; set; }
    }
}
