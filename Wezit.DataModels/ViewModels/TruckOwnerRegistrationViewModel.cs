﻿using Wezit.DataModels.DTO;

namespace Wezit.DataModels.ViewModels
{
    public class TruckOwnerRegistrationViewModel:TruckOwnerDTO
    {
        public TruckCompanySubscriptionDTO TruckCompanySubscription { get; set; }
    }
}
